<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Venta</title>
</head>
<body>
	<div>
		<h1>Aqu� Estan las Ventas Realizadas</h1>
		<hr>
		<table>
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombre Venta</th>
					<th>Cantidad</th>
					<th>Precio Total</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="venta" items="${lista_ventas}">
					<tr>
						<td><c:out value="${venta.id}"></c:out></td>
						<td><c:out value="${venta.nombre}"></c:out></td>
						<td><c:out value="${venta.cantidad}"></c:out></td>
						<td><c:out value="${venta.precioTotal}"></c:out></td>
						<td> 
							<form action="/venta/eliminar/${venta.id}" method="post">
								<input type="hidden" name="_method" value="delete">
								<input type="submit" value="Eliminar">
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<hr>
	</div>
</body>
</html>