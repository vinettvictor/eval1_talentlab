package com.everis.data.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.everis.data.models.Venta;
import com.everis.data.services.VentaService;

@Controller
@RequestMapping("/venta")
public class VentaController {

	@Autowired
	private VentaService ventaService;
	
	@RequestMapping("")
	public String index(@ModelAttribute("venta") Venta venta, Model model) {
		ArrayList<Venta> lista_ventas = new ArrayList<Venta>(); 
		
		//creamos 3 objetos de ventas
		Venta venta1 = new Venta("Venta numero 1",1000000,250000000);
		Venta venta2 = new Venta("Venta numero 2",2000000,1050000000);
		Venta venta3 = new Venta("Venta numero 3",3000000,550000000);
		
		//almacenamos los objetos ventas en la colleción lista_ventas
		lista_ventas.add(venta1);
		lista_ventas.add(venta2);
		lista_ventas.add(venta3);
		
		//Almacenamos las ventas en la base de datos
		ventaService.crearVenta(venta1);
		ventaService.crearVenta(venta2);
		ventaService.crearVenta(venta3);
		//List<Venta> lista_ventas = ventaService.findAll();
		model.addAttribute("lista_ventas", lista_ventas);
		
		return "venta.jsp";
	}
	
	@RequestMapping(value="/eliminar/{id}", method = RequestMethod.DELETE)
	public String eliminarVenta(@PathVariable("id") Long id) {
		System.out.println("El id a eliminar es: "+id);
		ventaService.eliminarVenta(id);
		return "redirect:/venta";
	}
}
