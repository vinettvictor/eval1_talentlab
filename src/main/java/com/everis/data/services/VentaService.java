package com.everis.data.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.everis.data.models.Venta;
import com.everis.data.repositories.VentaRepository;

@Service
public class VentaService {

	private final  VentaRepository ventaRepository;
	public VentaService(VentaRepository ventaRepository) {
		this.ventaRepository = ventaRepository; 
	}
	
	public void crearVenta(@Valid Venta venta) {			
		ventaRepository.save(venta);
	}
	
	public List<Venta> findAll() {
		return ventaRepository.findAll();
	}	
	
	public void eliminarVenta(Long id) {
		ventaRepository.deleteById(id);		
	}
	
	public Venta buscarVenta(Long id) {
		Optional<Venta> oVenta = ventaRepository.findById(id);
		if(oVenta.isPresent()) {
			return oVenta.get();
		}
		return null;
	}
	
	public void modificarVenta(@Valid Venta venta) {	
		ventaRepository.save(venta);
	}
}
